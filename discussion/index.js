function displayMsgToSelf(){
    console.log("Don't text her back");
};

//Invoke function 10 times

// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();

//WHILE LOOP

let count = 10;

while(count !== 0){
    displayMsgToSelf();
    count--;
};

//while loop
    //While Loop will allow us to repeat an action or an instruction as long as the condition is true

/*

    while loop checked if count is still NOT equal to 0:
    at this point, before a possible 11th loop, count was decremented to 0 therefore, there was no 11th loop


    If there is no decrementation, the condition is always true, thus, an infinite loop

    Infinite loops will run our code block forever until you stop it.

*/


//Mini activity
let x = 5;

while(x <= 1){
    console.log(x);
    x--;
}

/*

    DO - WHILE LOOP
        Do-while loop is similar to the While loop. However, Do-while loop will allow us to run our loop at least once.

        With the while, we check the condition first before running our  code block, however for the do-while loop, it will do an instruction first before it will check the condition to run again. 


 */

        let doWhileCounter = 1;

        do{

            console.log(doWhileCounter);

        } while (doWhileCounter === 0){
            console.log(doWhileCounter);
            doWhileCounter--;
        }


//Mini-Activity

        let A = 1;
        do{
            console.log(A);
            A++;
        } while (A <= 20);

for (let count = 0; count <= 20; count++){
    console.log(count);
};

let fruits = ["Apple", "Durian", "Kiwi", "Pineapple", "Strawberry"];
console.log(fruits[2]);
console.log(fruits[1]);
console.log(fruits[0]);
console.log(fruits[4]);

    console.log(fruits.length);
    console.log(fruits[fruits.length-1]);

for(let index = 0; index <fruits.length; index++);
console.log(fruits);


let country = ["Philippines", "Japan", "Korea", "America", "China", "Europe"];
for (let x = 0; x < country.length -1; x++)
{
    console.log(country[x]);
};







